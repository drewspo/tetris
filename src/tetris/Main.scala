package tetris

import scalafx.Includes.eventClosureWrapperWithParam
import scalafx.Includes.jfxKeyEvent2sfx
import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafx.scene.canvas.Canvas
import scalafx.scene.canvas.GraphicsContext
import scalafx.scene.input.KeyCode
import scalafx.scene.input.KeyEvent
import scalafx.scene.paint.Color
import scala.actors.Actor
import scala.actors.Future
import scala.actors.Future
import scala.concurrent.Future
import scala.concurrent.blocking
import scalafx.geometry.Rectangle2D
import scalafx.scene.text.Text
import scalafx.scene.text.Font
import scalafx.scene.text.FontWeight
import scala.collection.mutable

object Main extends JFXApp {
  def cellWidth = 30
  def xOffset = 150
  def yOffset = 75
  val canvas = new Canvas(600, 750)
  val gc = canvas.graphicsContext2D
  val pieces = new DLinkedList[Block]()
  var fallingBlock: Block = null
  private var locked = false
  def isLocked = locked
  private var time = System.currentTimeMillis
  private var paused = false
  private var blockQueue = new mutable.ArrayBuffer[Int]()
  private var swapBlock: Block = null
  private var canSwap = true
  def lock = {
    locked = true
    plug += 1
    GameEngine.addToGrid(fallingBlock)
    GameEngine.checkClear
    newBlock(blockQueue(0))
    newQueueBlock
    canSwap = true
  }
  private var speed = 500
  private var linesCleared = 0
  private var level = 1
  private var numBlocks = 0
  val r = scala.util.Random
  stage = new JFXApp.PrimaryStage {
    title = "TETRIS"
    scene = new Scene(600, 750) {
      content = List(canvas)
      onKeyPressed = (e: KeyEvent) => {
        e.code match {
          case KeyCode.LEFT  => if (!locked && !paused) fallingBlock.action(1)
          case KeyCode.RIGHT => if (!locked && !paused) fallingBlock.action(2)
          case KeyCode.UP    => if (!locked && !paused) tryTurn
          case KeyCode.DOWN  => if (!locked && !paused) fallingBlock.action(0)
          case KeyCode.SPACE => if (!locked && !paused) fastDrop
          case KeyCode.S     => //start
          case KeyCode.P     => pauseGame
          case KeyCode.C     => if (canSwap && !locked && !paused) blockSwap
          case _             =>
        }
      }
    }
  }
  def pauseGame = {
    if (paused) {
      paused = false
      if (!locked) startFalling
    } else {
      paused = true
    }
  }
  def tryTurn = {
    if (System.currentTimeMillis - time > .300 && !locked) {
      fallingBlock.action(3)
      time = System.currentTimeMillis
    }
  }
  val drawGame = new Thread(new Runnable {
    def run() {
      while (true) {
        if (pieces.length % 10 == 0) removeEmptyPieces
        drawBoard
        drawScore
        drawGrid
        drawPieces
        drawQueue
        Thread.sleep(50)
      }
    }
  })
  val lineColor = Color.WhiteSmoke
  def drawBoard = {
    gc.fill = Color.BlueViolet
    gc.fillRect(0, 0, 600, 750)
    gc.fill = Color.White
    gc.fillRect(460, 75, 120, 3)
    gc.fillRect(460, 167, 120, 3)
    gc.fillRect(460, 75, 3, 93)
    gc.fillRect(580, 75, 3, 96)
    gc.fill = Color.White
    gc.font = new Font("Chalkduster", 40)
    gc.fillText("TETRIS", 220, 48)
  }
  def drawGrid = {
    gc.fill = Color.Black
    gc.fillRect(150, 75, 300, 600)
    gc.fill = Color.SteelBlue
    for (i <- 0 to 10) {
      gc.fillRect(xOffset + i * cellWidth, yOffset, 1, 600)
    }
    for (i <- 0 to 20) {
      gc.fillRect(xOffset, yOffset + i * cellWidth, 300, 1)
    }
  }
  def drawPieces = {
    for (i <- 0 until GameEngine.map.length; j <- 0 until GameEngine.map(0).length) {
      if (GameEngine.map(i)(j) > 0) {
        GameEngine.map(i)(j) match {
          case 1 => gc.fill = Color.DeepSkyBlue
          case 2 => gc.fill = Color.Yellow
          case 3 => gc.fill = Color.LimeGreen
          case 4 => gc.fill = Color.Red
          case 5 => gc.fill = Color.Blue
          case 6 => gc.fill = Color.Orange
          case 7 => gc.fill = Color.Purple
        }
        gc.fillRect(j * cellWidth + xOffset, i * cellWidth + yOffset, cellWidth, cellWidth)
        //gc.fill = Color.White
        //gc.fillRect(j * cellWidth + xOffset, i * cellWidth + yOffset, cellWidth, 2)
        //gc.fillRect(j * cellWidth + xOffset, i * cellWidth + yOffset, 2, cellWidth)
        //gc.fillRect(j * cellWidth + xOffset + cellWidth, i * cellWidth + yOffset, 2, cellWidth + 2)
        //gc.fillRect(j * cellWidth + xOffset, i * cellWidth + yOffset + cellWidth, cellWidth + 2, 2)
      }
    }
    gc.fill = fallingBlock.color
    for (i <- 0 until fallingBlock.tiles.length) {
      gc.fillRect(fallingBlock.tiles(i).x * cellWidth + xOffset, fallingBlock.tiles(i).y * cellWidth + yOffset, cellWidth, cellWidth)
    }
    //drawLines(fallingBlock)
  }
  def drawScore = {
    gc.fill = Color.BlueViolet
    gc.fillRect(467, 82, 110, 82)
    gc.fill = Color.White
    gc.font = new Font("Chalkduster", 15)
    gc.fillText("Level: " + (linesCleared / 10 + 1), 470, 100)
    gc.fillText("Cleared: " + linesCleared, 470, 160)
  }
  def drawBlock(block: Block) = {
    gc.fill = block.color
    for (i <- 0 until block.tiles.length) {
      gc.fillRect(block.tiles(i).x * cellWidth + xOffset, block.tiles(i).y * cellWidth + yOffset, cellWidth, cellWidth)
    }
  }
  def drawLines(block: Block) = {
    gc.fill = Color.White
    for (i <- 0 until block.tiles.length) {
      gc.fillRect(block.tiles(i).x * cellWidth + xOffset, block.tiles(i).y * cellWidth + yOffset, cellWidth, 2)
      gc.fillRect(block.tiles(i).x * cellWidth + xOffset, block.tiles(i).y * cellWidth + yOffset, 2, cellWidth)
      gc.fillRect(block.tiles(i).x * cellWidth + xOffset + cellWidth, block.tiles(i).y * cellWidth + yOffset, 2, cellWidth + 2)
      gc.fillRect(block.tiles(i).x * cellWidth + xOffset, block.tiles(i).y * cellWidth + yOffset + cellWidth, cellWidth + 2, 2)
    }
  }
  val queueCell = 20
  val queueLength = 50
  def drawQueue = {
    gc.fill = Color.White
    for (i <- blockQueue) i match {
      case 0 => {
        gc.fillRect(50, yOffset + i*queueLength, queueCell, queueCell) 
      }
      case 1 => {
        
      }
      case 2 => {
        
      }
      case 3 => {
        
      }
      case 4 => {
        
      }
      case 5 => {
        
      }
      case 6 => {
        
      }
    }
  }
  def newBlock(n: Int): Unit = n match {
    case 0 => initialize(GameEngine.line)
    case 1 => initialize(GameEngine.square)
    case 2 => initialize(GameEngine.upstair)
    case 3 => initialize(GameEngine.downstair)
    case 4 => initialize(GameEngine.leftarm)
    case 5 => initialize(GameEngine.rightarm)
    case 6 => initialize(GameEngine.tee)
    case _ =>
  }
  private var plug = 0
  def initialize(block: Block) = {
    pieces.add(block, 0)
    numBlocks += 1
    fallingBlock = block
    locked = false
    val tmp = numBlocks
    val fall = new Thread(new Runnable {
      override def run() = {
        while (tmp == numBlocks && !paused) {
          Thread.sleep(speed)
          fallingBlock.action(0)
        }
      }
    })
    fall.start
  }
  def startFalling = {
    val tmp = numBlocks
    val fall = new Thread(new Runnable {
      override def run() = {
        while (tmp == numBlocks && !paused) {
          Thread.sleep(speed)
          fallingBlock.action(0)
        }
      }
    })
    fall.start
  }

  def removeRow(row: Int): Unit = {
    Thread.sleep(500)
    for (i <- 0 until pieces.length) {
      for (j <- pieces(i).tiles.length - 1 to 0 by -1) {
        if (pieces(i).tiles(j).y == row) {
          pieces(i).tiles.remove(j)
        }
      }
    }
    for (i <- 0 until pieces.length) {
      for (j <- 0 until pieces(i).tiles.length) {
        if (pieces(i).tiles(j).y < row) {
          pieces(i).tiles(j).y += 1
        }
      }
    }
    linesCleared += 1
    drawScore
  }
  def removeEmptyPieces = {
    for (i <- pieces.length - 1 to 0 by -1) {
      if (pieces(i).tiles.length < 1) {
        pieces.remove(i)
      }
    }
  }
  def fastDrop: Unit = {
    while (GameEngine.blocksClear(fallingBlock, Pieces.down)) {
      fallingBlock.action(0)
    }
  }
  def start = {
    for (i <- 0 until 3) {
      blockQueue += r.nextInt(7)
    }
    newBlock(blockQueue(0))
    newQueueBlock
  }
  def newQueueBlock = {
    blockQueue(0) = blockQueue(1)
    blockQueue(1) = blockQueue(2)
    blockQueue(2) = r.nextInt(7)
  }
  def blockSwap = {
    if (swapBlock == null && !locked) {
      locked = true
      canSwap = false
      swapBlock = fallingBlock
      pieces.remove(0)
      newBlock(blockQueue(0))
      newQueueBlock
      locked = false
    } else {
      locked = true
      canSwap = false
      pieces.remove(0)
      val tmp = swapBlock
      swapBlock = fallingBlock
      fallingBlock = tmp
      newBlock(fallingBlock.shape - 1)
      locked = false
    }
  }
  start
  drawGame.start
}


