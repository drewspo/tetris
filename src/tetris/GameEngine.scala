package tetris

object GameEngine {
  def line: Line = {
    val tiles = new DLinkedList[Tile]
    tiles.add(Tile(3, 0), 0)
    tiles.add(Tile(4, 0), 1)
    tiles.add(Tile(5, 0), 2)
    tiles.add(Tile(6, 0), 3)
    new Line(tiles)
  }
  def square: Square = {
    val tiles = new DLinkedList[Tile]
    tiles.add(Tile(4, 0), 0)
    tiles.add(Tile(5, 0), 1)
    tiles.add(Tile(4, 1), 2)
    tiles.add(Tile(5, 1), 3)
    new Square(tiles)
  }
  def upstair: Upstair = {
    val tiles = new DLinkedList[Tile]
    tiles.add(Tile(4, 1), 0)
    tiles.add(Tile(5, 1), 1)
    tiles.add(Tile(5, 0), 2)
    tiles.add(Tile(6, 0), 3)
    new Upstair(tiles)
  }
  def downstair: Downstair = {
    val tiles = new DLinkedList[Tile]
    tiles.add(Tile(4, 0), 0)
    tiles.add(Tile(5, 0), 1)
    tiles.add(Tile(5, 1), 2)
    tiles.add(Tile(6, 1), 3)
    new Downstair(tiles)
  }
  def leftarm: LeftArm = {
    val tiles = new DLinkedList[Tile]
    tiles.add(Tile(4, 0), 0)
    tiles.add(Tile(4, 1), 1)
    tiles.add(Tile(5, 1), 2)
    tiles.add(Tile(6, 1), 3)
    new LeftArm(tiles)
  }
  def rightarm: RightArm = {
    val tiles = new DLinkedList[Tile]
    tiles.add(Tile(4, 1), 0)
    tiles.add(Tile(5, 1), 1)
    tiles.add(Tile(6, 1), 2)
    tiles.add(Tile(6, 0), 3)
    new RightArm(tiles)
  }
  def tee: Tee = {
    val tiles = new DLinkedList[Tile]
    tiles.add(Tile(5, 0), 0)
    tiles.add(Tile(4, 1), 1)
    tiles.add(Tile(5, 1), 2)
    tiles.add(Tile(6, 1), 3)
    new Tee(tiles)
  }
  private var _linesCleared = 0
  def linesCleared = _linesCleared

  private var grid = Array(Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
  def map = grid

  def addToGrid(block: Block) = {
    for (i <- 0 until 4) {
      grid(block.tiles(i).y)(block.tiles(i).x) = block.shape
    }
  }
  def blocksClear(block: Block, offsets: Array[Int]): Boolean = {
    var ret = true
    for (i <- 0 until 4) {
      if (block.tiles(i).y + offsets(i * 2 + 1) < 0 || block.tiles(i).y + offsets(i * 2 + 1) > 19 || block.tiles(i).x + offsets(i * 2) < 0 || block.tiles(i).x + offsets(i * 2) > 9) {
        ret = false
      } else if (grid(block.tiles(i).y + offsets(i * 2 + 1))(block.tiles(i).x + offsets(i * 2)) > 0) {
        ret = false
      }
    }
    ret
  }
  def adjust(a: Array[Int], dir: Int, mag: Int): Array[Int] = dir match {
    case 1 => {
      Array(a(0) + mag, a(1), a(2) + mag, a(3), a(4) + mag, a(5), a(6) + mag, a(7))
    }
    case 2 => {
      Array(a(0), a(1) + mag, a(2), a(3) + mag, a(4), a(5) + mag, a(6), a(7) + mag)
    }
  }

  def checkClear: Unit = {
    for (i <- 0 to grid.length - 1) {
      if (grid(i).forall(_ > 0)) {
        Main.removeRow(i)
        for (j <- 0 until grid(i).length) grid(i)(j) = 0
        for (j <- i to 0 by -1) {
          for (h <- 0 until grid(j).length) {
            if (grid(j)(h) > 0) {
              grid(j + 1)(h) = grid(j)(h)
              grid(j)(h) = 0
            }
          }
        }
        _linesCleared += 1
      }
    }
  }
  
  

}