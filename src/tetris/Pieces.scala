package tetris
import tetris.Pieces._

import scalafx.scene.canvas.GraphicsContext
import scalafx.scene.paint.Color

class Line(squares: DLinkedList[Tile]) extends Block {
  def shape = 1
  def color = Color.DeepSkyBlue
  def tiles = squares
  def turned = state match {
    case 1 => {
      state = 2
    }
    case 2 => {
      state = 3
    }
    case 3 => {
      state = 4
    }
    case 4 => {
      state = 1
    }
  }
  private var state = 1
  def action(n: Int): Unit = squares synchronized {
    n match {
      case 0 => if(!Main.isLocked) drop
      case 1 => moveLeft
      case 2 => moveRight
      case 3 => turning
      case 4 => 
      case _ =>
    }
  }
  def turning: Unit = state match {
    case 1 => {
      turn(lineDown)
    }
    case 2 => {
      turn(lineLeft)
    }
    case 3 => {
      turn(lineUp)
    }
    case 4 => {
      turn(lineRight)
    }
  }
}

class Square(squares: DLinkedList[Tile]) extends Block {
  def shape = 2
  def color = Color.Yellow
  def tiles = squares
  def turned: Unit = ???
  private var state = 1
  def action(n: Int): Unit = squares synchronized {
    n match {
      case 0 => if(!Main.isLocked) drop
      case 1 => moveLeft
      case 2 => moveRight
      case 4 => 
      case _ =>
    }
  }
}

class Upstair(squares: DLinkedList[Tile]) extends Block {
  def shape = 3
  def color = Color.LimeGreen
  def tiles = squares
  private var state = 1
  def action(n: Int): Unit = squares synchronized {
    n match {
      case 0 => if(!Main.isLocked) drop
      case 1 => moveLeft
      case 2 => moveRight
      case 3 => turning
      case 4 => 
      case _ =>
    }
  }
  def turned = state match {
    case 1 => {
      state = 2
    }
    case 2 => {
      state = 1
    }
  }
  def turning: Unit = state match {
    case 1 => {
      turn(upstairTwo)
    }
    case 2 => {
      turn(upstairOne)
    }
  }
}

class Downstair(squares: DLinkedList[Tile]) extends Block {
  def shape = 4
  def color = Color.Red
  def tiles = squares
  private var state = 1
  def action(n: Int): Unit = squares synchronized {
    n match {
      case 0 => if(!Main.isLocked) drop
      case 1 => moveLeft
      case 2 => moveRight
      case 3 => turning
      case 4 => 
      case _ =>
    }
  }
  def turned = state match {
    case 1 => {
      state = 2
    }
    case 2 => {
      state = 1
    }
  }
  def turning: Unit = state match {
    case 1 => {
      turn(downstairOne)
    }
    case 2 => {
      turn(downstairTwo)
    }
  }
}
class LeftArm(squares: DLinkedList[Tile]) extends Block {
  def shape = 5
  def color = Color.Blue
  def tiles = squares
  def turned = state match {
    case 1 => {
      state = 2
    }
    case 2 => {
      state = 3
    }
    case 3 => {
      state = 4
    }
    case 4 => {
      state = 1
    }
  }
  private var state = 1
  def action(n: Int): Unit = squares synchronized {
    n match {
      case 0 => if(!Main.isLocked) drop
      case 1 => moveLeft
      case 2 => moveRight
      case 3 => turning
      case 4 => 
      case _ =>
    }
  }
  def turning: Unit = state match {
    case 1 => {
      turn(leftarmUp)
    }
    case 2 => {
      turn(leftarmRight)
    }
    case 3 => {
      turn(leftarmDown)
    }
    case 4 => {
      turn(leftarmLeft)
    }
  }
}
class RightArm(squares: DLinkedList[Tile]) extends Block {
  def shape = 6
  def color = Color.Orange
  def tiles = squares
  def turned = state match {
    case 1 => {
      state = 2
    }
    case 2 => {
      state = 3
    }
    case 3 => {
      state = 4
    }
    case 4 => {
      state = 1
    }
  }
  private var state = 1
  def action(n: Int): Unit = squares synchronized {
    n match {
      case 0 => if(!Main.isLocked) drop
      case 1 => moveLeft
      case 2 => moveRight
      case 3 => turning
      case 4 => 
      case _ =>
    }
  }
  def turning: Unit = state match {
    case 1 => {
      turn(rightarmDown)
    }
    case 2 => {
      turn(rightarmLeft)
    }
    case 3 => {
      turn(rightarmUp)
    }
    case 4 => {
      turn(rightarmRight)
    }
  }
}

class Tee(squares: DLinkedList[Tile]) extends Block {
  def shape = 7
  def color = Color.Purple
  def tiles = squares
  def turned = state match {
    case 1 => {
      state = 2
    }
    case 2 => {
      state = 3
    }
    case 3 => {
      state = 4
    }
    case 4 => {
      state = 1
    }
  }
  private var state = 1
  def action(n: Int): Unit = squares synchronized {
    n match {
      case 0 => if(!Main.isLocked) drop
      case 1 => moveLeft
      case 2 => moveRight
      case 3 => turning
      case 4 => 
      case _ =>
    }
  }
  def turning: Unit = state match {
    case 1 => {
      turn(teeLeft)
    }
    case 2 => {
      turn(teeUp)
    }
    case 3 => {
      turn(teeRight)
    }
    case 4 => {
      turn(teeDown)
    }
  }
}

object Pieces {
  val left = Array(-1, 0, -1, 0, -1, 0, -1, 0)
  val right = Array(1, 0, 1, 0, 1, 0, 1, 0)
  val down = Array(0, 1, 0, 1, 0, 1, 0, 1)
  val lineDown = Array(Array(1, -2, 0, -1, -1, 0, -2, 1),
    GameEngine.adjust(Array(1, -2, 0, -1, -1, 0, -2, 1), 2, -1))
  val lineLeft = Array(Array(2, 2, 1, 1, 0, 0, -1, -1),
    GameEngine.adjust(Array(2, 2, 1, 1, 0, 0, -1, -1), 1, 1),
    GameEngine.adjust(Array(2, 2, 1, 1, 0, 0, -1, -1), 1, 2),
    GameEngine.adjust(Array(2, 2, 1, 1, 0, 0, -1, -1), 1, -1),
    GameEngine.adjust(Array(2, 2, 1, 1, 0, 0, -1, -1), 1, -2))
  val lineUp = Array(Array(-1, 1, 0, 0, 1, -1, 2, -2),
    GameEngine.adjust(Array(-1, 1, 0, 0, 1, -1, 2, -2), 2, -1))
  val lineRight = Array(Array(-2, -1, -1, 0, 0, 1, 1, 2),
    GameEngine.adjust(Array(-2, -1, -1, 0, 0, 1, 1, 2), 1, 1),
    GameEngine.adjust(Array(-2, -1, -1, 0, 0, 1, 1, 2), 1, 2),
    GameEngine.adjust(Array(-2, -1, -1, 0, 0, 1, 1, 2), 1, -1),
    GameEngine.adjust(Array(-2, -1, -1, 0, 0, 1, 1, 2), 1, -2))
  val upstairOne = Array(Array(0, 2, 1, 1, 0, 0, 1, -1),
    GameEngine.adjust(Array(0, 2, 1, 1, 0, 0, 1, -1), 1, -1),
    GameEngine.adjust(Array(0, 2, 1, 1, 0, 0, 1, -1), 1, 1))
  val upstairTwo = Array(Array(0, -2, -1, -1, 0, 0, -1, 1),
    GameEngine.adjust(Array(0, -2, -1, -1, 0, 0, -1, 1), 1, 1),
    GameEngine.adjust(Array(0, -2, -1, -1, 0, 0, -1, 1), 1, -1))
  val downstairOne = Array(Array(1, -1, 0, 0, -1, -1, -2, 0),
    GameEngine.adjust(Array(1, -1, 0, 0, -1, -1, -2, 0), 1, 1),
    GameEngine.adjust(Array(1, -1, 0, 0, -1, -1, -2, 0), 1, -1))
  val downstairTwo = Array(Array(-1, 1, 0, 0, 1, 1, 2, 0),
    GameEngine.adjust(Array(-1, 1, 0, 0, 1, 1, 2, 0), 1, 1),
    GameEngine.adjust(Array(-1, 1, 0, 0, 1, 1, 2, 0), 1, -1))
  val leftarmUp = Array(Array(1, -1, 0, -2, -1, -1, -2, 0))
  val leftarmRight = Array(Array(1, 2, 2, 1, 1, 0, 0, -1),
    GameEngine.adjust(Array(1, 2, 2, 1, 1, 0, 0, -1), 1, 1),
    GameEngine.adjust(Array(1, 2, 2, 1, 1, 0, 0, -1), 1, -1))
  val leftarmDown = Array(Array(-1, 0, 0, 1, 1, 0, 2, -1))
  val leftarmLeft = Array(Array(-1, -1, -2, 0, -1, 1, 0, 2),
    GameEngine.adjust(Array(-1, -1, -2, 0, -1, 1, 0, 2), 1, 1),
    GameEngine.adjust(Array(-1, -1, -2, 0, -1, 1, 0, 2), 1, -1))
  val rightarmDown = Array(Array(1, -2, 0, -1, -1, 0, 0, 1))
  val rightarmLeft = Array(Array(2, 1, 1, 0, 0, -1, -1, 0),
    GameEngine.adjust(Array(2, 1, 1, 0, 0, -1, -1, 0), 1, 1),
    GameEngine.adjust(Array(2, 1, 1, 0, 0, -1, -1, 0), 1, -1))
  val rightarmUp = Array(Array(-1, 1, 0, 0, 1, -1, 0, -2))
  val rightarmRight = Array(Array(-2, 0, -1, 1, 0, 2, 1, 1),
    GameEngine.adjust(Array(-2, 0, -1, 1, 0, 2, 1, 1), 1, 1),
    GameEngine.adjust(Array(-2, 0, -1, 1, 0, 2, 1, 1), 1, -1))
  val teeLeft = Array(Array(0, 0, 0, -2, -1, -1, -2, 0),
      GameEngine.adjust(Array(0, 0, 0, -2, -1, -1, -2, 0), 2, -1))
  val teeUp = Array(Array(0, 0, 2, 0, 1, -1, 0, -2),
      GameEngine.adjust(Array(0, 0, 2, 0, 1, -1, 0, -2), 1, 1),
      GameEngine.adjust(Array(0, 0, 2, 0, 1, -1, 0, -2), 1, -1))
  val teeRight = Array(Array(0, 0, 0, 2, 1, 1, 2, 0),
      GameEngine.adjust(Array(0, 0, 0, 2, 1, 1, 2, 0), 2, -1))
  val teeDown = Array(Array(0, 0, -2, 0, -1, 1, 0, 2), 
      GameEngine.adjust(Array(0, 0, -2, 0, -1, 1, 0, 2), 1, -1), 
      GameEngine.adjust(Array(0, 0, -2, 0, -1, 1, 0, 2), 1, 1))
}
