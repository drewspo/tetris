package tetris

import scalafx.scene.paint.Color
import scalafx.scene.canvas.GraphicsContext

trait Block {
  def shape:Int
  def tiles: DLinkedList[Tile]
  def color: Color
  def lock: Unit = Main.lock
  def action(n: Int): Unit
  def turned: Unit
  def moveLeft: Unit = {
    if (GameEngine.blocksClear(this, Pieces.left)) {
      for (i <- 0 until tiles.length) {
        tiles(i).x -= 1
      }
    }
  }
  def moveRight: Unit = {
    if (GameEngine.blocksClear(this, Pieces.right)) {
      for (i <- 0 until tiles.length) {
        tiles(i).x += 1
      }
    }
  }
  def drop: Unit = {
    if (GameEngine.blocksClear(this, Pieces.down)) {
      for (i <- 0 until tiles.length) {
        tiles(i).y += 1
      }
    } else {
      lock
    }
  }
  def turn(offsets: Array[Array[Int]]): Unit = {
    var flag = true
    var index = 0
    while (flag && index < offsets.length) {
      if (GameEngine.blocksClear(this, offsets(index))) {
        for (i <- 0 until tiles.length) {
          tiles(i).x += offsets(index)(i * 2); tiles(i).y += offsets(index)(i * 2 + 1)
        }
        this.turned
        flag = false
      } else {
        index += 1
      }
    }
  }


}

case class Tile(var x: Int, var y: Int)
case class Offsets(x1: Int, y1: Int, x2: Int, y2: Int, x3: Int, y3: Int, x4: Int, y4: Int)
case class MoveDown(offsets: Array[Int])
case class MoveLeft(offsets: Array[Int])
case class MoveRight(offsets: Array[Int])
case class Turn(offsets: Array[Int])
